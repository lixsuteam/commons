package com.felixsu.commons.model;

import java.io.Serializable;

/**
 * Created on 12/7/17.
 *
 * @author felixsoewito
 */
public interface EntityModel<ID extends Serializable> {

    ID getId();
}
