package com.felixsu.commons.model;

import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Created on 4/24/17.
 *
 * @author felixsoewito
 */
public class PagingWrapper<T> {
    private List<T> datas;
    private long totalPages;
    private long totalElement;
    private boolean first;
    private boolean last;

    public static <T> PagingWrapper<T> wrap(Page<T> page) {
        PagingWrapper result = new PagingWrapper();
        result.setDatas(page.getContent());
        result.setTotalElement(page.getTotalElements());
        result.setTotalPages(page.getTotalPages());
        result.setFirst(page.isFirst());
        result.setLast(page.isLast());
        return result;
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }

    public long getTotalElement() {
        return totalElement;
    }

    public void setTotalElement(long totalElement) {
        this.totalElement = totalElement;
    }

    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }
}
