package com.felixsu.commons.model;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created on 12/7/17.
 *
 * @author felixsoewito
 */
public class ErrorWrapper {
    private int code;
    private String message;
    private String stackTrace;

    public static ErrorWrapper fromThrowable(int code, Throwable t) {
        StringWriter sw = new StringWriter();
        t.printStackTrace(new PrintWriter(sw));
        return new ErrorWrapper(code, t.getMessage(), sw.toString());
    }

    public ErrorWrapper() {
    }

    public ErrorWrapper(int code, String message, String stackTrace) {
        this.code = code;
        this.message = message;
        this.stackTrace = stackTrace;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }
}
