package com.felixsu.commons.service;

import com.felixsu.commons.model.EntityModel;
import com.felixsu.commons.util.PredicateBuilder;
import com.felixsu.commons.util.ReflectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * Created on 12/13/17.
 *
 * @author felixsoewito
 */
public class BaseService<T extends EntityModel<ID>, ID extends Serializable> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseService.class);

    final protected Class<T> entityClass;

    public BaseService(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected Specification<T> buildSpec(Map<String, String> queryStrings) {
        return (root, query, cb) -> {
            PredicateBuilder<T> builder = PredicateBuilder.getDefaultBuilder(root, query, cb, entityClass);
            final Set<String> fieldNames = ReflectionUtil.getFields(entityClass);
            for (Map.Entry<String, String> entry : queryStrings.entrySet()) {
                final String key = entry.getKey();
                if (fieldNames.contains(key)) {
                    try {
                        Class<?> clazz = ReflectionUtil.getTypeOfField(key, entityClass);
                        if (clazz.isAssignableFrom(String.class)) {
                            builder.addEqualString(key, entry.getValue());
                        } else if (clazz.isAssignableFrom(Integer.class)) {
                            Integer val = Integer.parseInt(entry.getValue());
                            builder.addEqualInteger(key, val);
                        } else if (clazz.isAssignableFrom(Boolean.class)) {
                            Boolean val = Boolean.parseBoolean(entry.getValue());
                            builder.addEqualBoolean(key, val);
                        }
                    } catch (Exception e) {
                        LOGGER.warn(e.getMessage(), e);
                    }
                }
            }
            return builder.getAndPredicate();
        };
    }

    /**
     *
     * @param object
     * @param current
     * @return current value if object is null
     */
    protected Object getNonNull(Object object, Object current) {
        return object != null ? object : current;
    }
}
