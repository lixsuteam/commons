package com.felixsu.commons.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.felixsu.commons.exception.BadRequestException;
import com.felixsu.commons.exception.NotFoundException;
import com.felixsu.commons.model.EntityModel;
import com.felixsu.commons.model.PagingWrapper;
import com.felixsu.commons.persistence.BaseRepository;
import com.felixsu.commons.util.ReflectionUtil;
import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

/**
 * Created on 12/7/17.
 *
 * @author felixsoewito
 */
public class SimpleService<R extends BaseRepository<T, ID>, T extends EntityModel<ID>, ID extends Serializable>
        extends BaseService<T, ID> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleService.class);

    protected R repository;

    public SimpleService(Class<T> entityClass, R repository) {
        super(entityClass);
        this.repository = repository;
    }

    public T save(T item) throws BadRequestException {
        if (item.getId() != null) {
            throw new BadRequestException("id must be null and not exist");
        }
        return repository.save(item);
    }

    public T update(ID id, T item) throws NotFoundException, BadRequestException {
        findOne(id);
        if (item.getId() == null) {
            throw new BadRequestException("ID must not be null");
        }
        return repository.save(item);
    }

    public T updatePartial(ID id, T item) throws NotFoundException, BadRequestException {
        T current = findOne(id);
        if (item.getId() == null) {
            throw new BadRequestException("ID must not be null");
        }
        try {
            LOGGER.debug(String.format("calling update partial for: %s", item.getClass()));
            List<Field> classFields = ReflectionUtil.getAllFields(current.getClass());
            for (Field field : classFields) {
                JsonProperty jsonProperty = field.getDeclaredAnnotation(JsonProperty.class);

                String setterName = jsonProperty != null
                        ? jsonProperty.value()
                        : field.getName();

                Object currentVal = PropertyUtils.getProperty(current, setterName);
                Object newVal = PropertyUtils.getProperty(item, setterName);
                PropertyUtils.setProperty(current, setterName, getNonNull(newVal, currentVal));
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return repository.save(item);
    }

    @NotNull
    public T delete(ID id) throws NotFoundException {
        T t = findOne(id);
        repository.delete(id);
        return t;
    }

    @NotNull
    public T findOne(ID id) throws NotFoundException {
        T t = repository.findOne(id);
        if (t == null) {
            throw new NotFoundException(String.format("Item with id #%d not found", id));
        }
        return t;
    }

    @SuppressWarnings("unchecked")
    public PagingWrapper<T> findAll(Map<String, String> queryStrings, Pageable pageable) {
        Page<T> result = repository.findAll(
                buildSpec(queryStrings),
                pageable == null ? new PageRequest(0, 20) : pageable);
        return PagingWrapper.wrap(result);
    }


}
