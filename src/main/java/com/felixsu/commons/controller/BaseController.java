package com.felixsu.commons.controller;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created on 12/13/17.
 *
 * @author felixsoewito
 */
public class BaseController {

    protected HttpServletRequest request;

    public BaseController(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * retrieve header into a map
     *
     * @return
     */
    protected Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        Enumeration en = request.getHeaderNames();
        while (en.hasMoreElements()) {
            String k = (String) en.nextElement();
            String v = request.getHeader(k);
            headers.put(k, v);

        }
        return headers;
    }

    /**
     * retrieve query params into a map
     *
     * @return
     */
    protected Map<String, String> getQueryParams() {
        Map<String, String> params = new HashMap<String, String>();
        Enumeration en = request.getParameterNames();
        while (en.hasMoreElements()) {
            String k = (String) en.nextElement();
            String v = request.getParameter(k);
            params.put(k, v);
        }
        return params;
    }
}
