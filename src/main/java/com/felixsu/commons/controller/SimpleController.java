package com.felixsu.commons.controller;

import com.felixsu.commons.exception.BadRequestException;
import com.felixsu.commons.exception.NotFoundException;
import com.felixsu.commons.model.EntityModel;
import com.felixsu.commons.model.PagingWrapper;
import com.felixsu.commons.persistence.BaseRepository;
import com.felixsu.commons.service.SimpleService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * Created on 12/7/17.
 *
 * @author felixsoewito
 */
public class SimpleController<S extends SimpleService<R, T, ID>, R extends BaseRepository<T, ID>, T extends EntityModel<ID>, ID extends Serializable> extends BaseController {

    protected S service;

    public SimpleController(HttpServletRequest request, S service) {
        super(request);
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.POST)
    public T save(@RequestBody T item) throws BadRequestException {
        return service.save(item);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}")
    public T update(@PathVariable ID id, @RequestBody T item) throws NotFoundException, BadRequestException {
        return service.update(id, item);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{id}")
    public T findOne(@PathVariable ID id) throws NotFoundException {
        return service.findOne(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public PagingWrapper<T> findAll() {
        return service.findAll(getQueryParams(), null);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public T delete(@PathVariable ID id) throws NotFoundException {
        return service.delete(id);
    }
}
