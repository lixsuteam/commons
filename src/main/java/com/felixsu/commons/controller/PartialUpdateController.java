package com.felixsu.commons.controller;

import com.felixsu.commons.exception.BadRequestException;
import com.felixsu.commons.exception.NotFoundException;
import com.felixsu.commons.model.EntityModel;
import com.felixsu.commons.persistence.BaseRepository;
import com.felixsu.commons.service.SimpleService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * Created on 12/25/17.
 *
 * @author felixsoewito
 */
public class PartialUpdateController<S extends SimpleService<R, T, ID>, R extends BaseRepository<T, ID>, T extends EntityModel<ID>, ID extends Serializable>
        extends SimpleController<S, R, T, ID> {

    public PartialUpdateController(HttpServletRequest request, S service) {
        super(request, service);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}")
    @Override
    public T update(ID id, T item) throws NotFoundException, BadRequestException {
        return service.updatePartial(id, item);
    }
}
