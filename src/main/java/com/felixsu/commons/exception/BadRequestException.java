package com.felixsu.commons.exception;

/**
 * Created on 12/7/17.
 *
 * @author felixsoewito
 */
public class BadRequestException extends Exception {

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadRequestException(Throwable cause) {
        super(cause);
    }
}
