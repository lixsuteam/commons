package com.felixsu.commons.exception;

/**
 * Created on 12/7/17.
 *
 * @author felixsoewito
 */
public class NotFoundException extends Exception {
    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(Throwable cause) {
        super(cause);
    }
}
