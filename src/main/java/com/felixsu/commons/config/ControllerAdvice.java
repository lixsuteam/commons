package com.felixsu.commons.config;

import com.felixsu.commons.exception.BadRequestException;
import com.felixsu.commons.exception.NotFoundException;
import com.felixsu.commons.model.ErrorWrapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerAdvice {

    private static final int STATUS_BAD_REQUEST = 400;
    private static final int STATUS_NOT_FOUND = 404;
    private static final int STATUS_INTERNAL_SERVER_ERROR = 500;

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorWrapper entityBadRequestExceptionHandler(BadRequestException e) {
        return ErrorWrapper.fromThrowable(STATUS_BAD_REQUEST, e);
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ErrorWrapper entityNotFoundExceptionHandler(NotFoundException e) {
        return ErrorWrapper.fromThrowable(STATUS_NOT_FOUND, e);
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ErrorWrapper entityInternalErrorExceptionHandler(RuntimeException e) {
        return ErrorWrapper.fromThrowable(STATUS_INTERNAL_SERVER_ERROR, e);
    }
}