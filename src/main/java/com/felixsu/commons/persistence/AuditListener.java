package com.felixsu.commons.persistence;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.Instant;

/**
 * Created on 12/23/17.
 *
 * @author felixsoewito
 */
public class AuditListener {

    @PrePersist
    public void onCreate(Object entity) {
        if (entity instanceof AuditBase) {
            ((AuditBase) entity).setCreatedAt(Instant.now());
        }
    }

    @PreUpdate
    public void onUpdate(Object entity) {
        if (entity instanceof AuditBase) {
            ((AuditBase) entity).setUpdatedAt(Instant.now());
        }
    }
}
