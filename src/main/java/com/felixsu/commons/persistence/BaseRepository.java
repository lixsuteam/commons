package com.felixsu.commons.persistence;

import com.felixsu.commons.model.EntityModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

/**
 * Created on 12/7/17.
 *
 * @author felixsoewito
 */
@NoRepositoryBean
public interface BaseRepository <T extends EntityModel<ID>, ID extends Serializable>
        extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {
}
