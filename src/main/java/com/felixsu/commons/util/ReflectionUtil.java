package com.felixsu.commons.util;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Created on 4/24/17.
 *
 * @author felixsoewito
 */
public class ReflectionUtil {

    public static Set<String> getFields(Class clazz) {
        Set<String> result = new HashSet<>();
        for (Field field : clazz.getDeclaredFields()) {
            result.add(field.getName());
        }
        return result;
    }

    public static Class<?> getTypeOfField(String fieldName, Class clazz) throws NoSuchFieldException {
        Field field = clazz.getDeclaredField(fieldName);
        field.setAccessible(true);
        return field.getType();
    }

    public static List<Field> getAllFields(Class clazz) {
        Class superClass = clazz.getSuperclass();
        if (superClass != null) {
            List<Field> result = new ArrayList<>();
            result.addAll(Arrays.asList(clazz.getDeclaredFields()));
            result.addAll(getAllFields(clazz.getSuperclass()));
            return result;
        } else {
            return Arrays.asList(clazz.getDeclaredFields());
        }
    }
}