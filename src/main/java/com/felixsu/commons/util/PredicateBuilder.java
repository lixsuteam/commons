package com.felixsu.commons.util;

import com.felixsu.commons.persistence.AuditBase;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class PredicateBuilder<T extends Object> {
    private List<Predicate> predicates = new ArrayList<>();
    private Root<T> root;
    private CriteriaQuery query;
    private CriteriaBuilder builder;

    public static <T> PredicateBuilder<T> getDefaultBuilder(Root<T> root, CriteriaQuery query, CriteriaBuilder builder, Class<T> clazz) {
        PredicateBuilder<T> b = new PredicateBuilder<>(root, query, builder);
        try {
            if (clazz.newInstance() instanceof AuditBase) {
                b.addIsNull("deletedAt");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }

    public PredicateBuilder(Root<T> root, CriteriaQuery query, CriteriaBuilder builder) {
        this.root = root;
        this.query = query;
        this.builder = builder;
    }

    public void addEqualInteger(String property, Integer value) {
        if (value != null) {
            predicates.add(builder.equal(root.get(property), value));
        }
    }

    public void addEqualString(String property, String value) {
        if (value != null) {
            predicates.add(builder.equal(root.get(property), value));
        }
    }

    public void addEqualBoolean(String property, Boolean value) {
        if (value != null) {
            predicates.add(builder.equal(root.get(property), value));
        }
    }

    public void addIsNull(String property) {
        predicates.add(builder.isNull(root.get(property)));
    }


    public Predicate getAndPredicate() {
        return builder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}