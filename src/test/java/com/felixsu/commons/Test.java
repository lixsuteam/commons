package com.felixsu.commons;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.felixsu.commons.model.Student;
import com.felixsu.commons.util.ReflectionUtil;
import org.apache.commons.beanutils.PropertyUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Created on 12/24/17.
 *
 * @author felixsoewito
 */
public class Test {

    public static void main(String[] args) throws Exception {
        Student student = new Student("2222222", "13");
        Student student2 = new Student("4356343", "12");
        student2.setAge(12);
        student2.setHeight(159.2);
        student2.setName("Felix");

        System.out.println("BEFORE");
        System.out.println(student2);

        System.out.println("===========");

        Class clazz = student.getClass();
        List<Field> classFields = ReflectionUtil.getAllFields(clazz);
        for (Field field : classFields) {
            System.out.println(field.getName());
            JsonProperty jsonProperty = field.getDeclaredAnnotation(JsonProperty.class);

            String setterName = jsonProperty != null
                    ? jsonProperty.value()
                    : field.getName();

            Object current = PropertyUtils.getProperty(student2, setterName);
            Object object = PropertyUtils.getProperty(student, setterName);
            PropertyUtils.setProperty(student2, setterName, object == null ? current : object);
        }

        System.out.println("AFTER");
        System.out.println(student2);

    }
}
