package com.felixsu.commons.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created on 12/24/17.
 *
 * @author felixsoewito
 */
public class Student extends Person {

    private String idCard;
    private String level;
    @JsonProperty("active")
    private Boolean isActive;

    public Student() {
    }

    public Student(String idCard, String level) {
        this.idCard = idCard;
        this.level = level;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "Student{" +
                "idCard='" + idCard + '\'' +
                ", level='" + level + '\'' +
                '}';
    }
}
